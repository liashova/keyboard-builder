using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

namespace KeyboardBuilder
{
    public class Spawner : MonoBehaviour
    {
        private List<Vector3> spawnLocations;
        public List<Keyboard> inventory = new List<Keyboard>();
        public int currentSlot;
        private CameraManager mainCam;

        GameObject[] editCanvas;

        void Start()
        {
            editCanvas = GameObject.FindGameObjectsWithTag("ShowOnEditOption");
            
            mainCam = Camera.main.GetComponent<CameraManager>();
            //get all keyboards 
            foreach (Keyboard k in Resources.LoadAll<Keyboard>(""))
            {
                inventory.Add(k);
            }
            Debug.Log(inventory.Count);
            if (SceneManager.GetActiveScene().name == "BuildScene")
            {
                mainCam = Camera.main.GetComponent<CameraManager>();
                spawnLocations = new List<Vector3>();
                
                //create spawn locations n = inventory size + 1 for Add
                for (int j = 0; j < inventory.Count + 1; j++)
                {
                    spawnLocations.Add(new Vector3(j * 1000, 0, 0));
                }

                //spawn all keyboards in scene + plus
                spawnPlus(spawnLocations[0]);
                for (int i = 1; i < inventory.Count + 1; i++)
                {
                    Debug.Log("spawner start is called");
                    spawnKeyboard(inventory[i-1].baseName, spawnLocations[i]);
                }
                
            }
            
        }
        void Update()
        {
            //Debug.Log("SLot in spawner " + mainCam.slot);
           if(mainCam.slot == 0)
            {
                hide(editCanvas);
            }
            else
            {
                show(editCanvas);
            }
        }

        public List<Vector3> GetSpawnLocs()
        {
            return spawnLocations;
        }
        public void applyProperties(Keyboard k)
        {

        }

        //public void createKeyboard()
        //{
        //    string clickedButton = EventSystem.current.currentSelectedGameObject.name;
        //    //Debug.Log("Button clicked! " + clickedButton);
        //    Keyboard newItem = ScriptableObject.CreateInstance<Keyboard>();
        //    newItem.baseName = clickedButton;
        //    AssetDatabase.CreateAsset(newItem, "Assets/Resources/K" + (inventory.Count+1) + ".asset");
        //    ReloadScene();

        //}

        public void spawnKeyboard(string name, Vector3 spawnLoc)
        {

            //Debug.Log("spawnKeyboard is called");
            Instantiate(Resources.Load("Prefabs/" + name + "Prefab"), spawnLoc, Quaternion.identity);

        }
        public void spawnPlus(Vector3 spawnLoc)
            
        {
            Instantiate(Resources.Load("Prefabs/Plus"), spawnLoc, Quaternion.identity);
            //model.transform.position += model.transform.position - gameObject.GetComponentInChildren<Renderer>().bounds.center;
        }

        void hide(GameObject[] canvas)
        {
            foreach (GameObject g in canvas)
            {
                g.SetActive(false);

            }
        }
        void show(GameObject[] canvas)
        {
            foreach (GameObject g in canvas)
            {
                g.SetActive(true);

            }
        }
         void ReloadScene()
        {
            Scene scene = SceneManager.GetActiveScene(); 
            SceneManager.LoadScene(scene.name);
        }

    }
}

