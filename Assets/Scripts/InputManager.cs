using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KeyboardBuilder
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField]
        public int touchArea;
       
        private CameraManager mainCam;
        private Vector2 startTouchPos;
        private Vector2 endTouchPos;

        GameObject[] createCanvas;
        bool touchEnabled = true;

       
        // Start is called before the first frame update
        void Start()
        {
            mainCam = Camera.main.GetComponent<CameraManager>();
            createCanvas = GameObject.FindGameObjectsWithTag("ShowOnCreateOption");
            hide(createCanvas);
        }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log("Touches " + Input.touchCount);
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                //Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                //RaycastHit raycastHit;
                //if (Physics.Raycast(raycast, out raycastHit))
                //{
                //    //Debug.Log("Something Hit! " + raycastHit.collider.name);
                //    if (raycastHit.collider.name == "Plus")
                //    {
                //        //Debug.Log("Soccer Ball clicked");
                //        show(createCanvas);
                //        touchEnabled = false;

                //    }
                //}
                    //Debug.Log("Swipe start");
                startTouchPos = Input.GetTouch(0).position;
            }
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended && touchEnabled)
            {
                endTouchPos = Input.GetTouch(0).position;

                if (endTouchPos.x < startTouchPos.x && (startTouchPos.x - endTouchPos.x) > touchArea)
                {
                    //Debug.Log("Swipe right");
                    mainCam.CameraMoveRight();
                }
                if (endTouchPos.x > startTouchPos.x && (endTouchPos.x - startTouchPos.x) > touchArea)
                {
                    //Debug.Log("Swipe left");
                    mainCam.CameraMoveLeft();
                }

            }

        }

        void hide(GameObject[] canvas)
        {
            foreach (GameObject g in canvas)
            {
                g.SetActive(false);

            }
        }
        void show(GameObject[] canvas)
        {
            foreach (GameObject g in canvas)
            {
                g.SetActive(true);

            }
        }

    }

    

}

