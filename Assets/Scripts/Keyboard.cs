using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KeyboardBuilder
{
    [CreateAssetMenu]
    [System.Serializable]
    public class Keyboard : ScriptableObject
    {
        List<string> BaseDB = new List<string>
        {
            "K60",
            "K75",
            "KFull",
            "KTKL"
        };
        [Dropdown("BaseDB")]
        public string baseName;
        public Color baseColor;
        public Color keycapColor;
    }
  
}
