using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace KeyboardBuilder
{
    public class EditManager : MonoBehaviour
    {
        [SerializeField]
        public Vector3 spawnLocation;
        public List<Keyboard> inventory = new List<Keyboard>();
        private int slotToEdit;
        // Start is called before the first frame update
        void Start()
        {
            slotToEdit = PlayerPrefs.GetInt("slot") - 1;
            Debug.Log("Current slot " + slotToEdit);

            foreach (Keyboard k in Resources.LoadAll<Keyboard>(""))
            {
                inventory.Add(k);
            }

            spawnKeyboard(inventory[slotToEdit].baseName, spawnLocation);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void spawnKeyboard(string name, Vector3 spawnLoc)
        {

            //Debug.Log("spawnKeyboard is called");
            Instantiate(Resources.Load("Prefabs/" + name + "Prefab"), spawnLoc, Quaternion.identity);

        }
    }
}

