using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIbuiltScene : MonoBehaviour
{
   public void onEditPressed()
    {
        SceneManager.LoadScene("EditorScene");
    }

   public void onARPressed()
    {
        SceneManager.LoadScene("SimulationScene");
    }
}
