using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace KeyboardBuilder
{
    public class CameraManager : MonoBehaviour
    {

        public float smoothSpeed = 0.125f; //TODO
        [SerializeField]
        public Vector3 offset;
        [SerializeField]
        public Vector3 rotOffset;
        Vector3 refPos;
        Vector3 position;
        Vector3 focus;
        public int slot;

        [SerializeField]
        public GameObject sp;
        Spawner spawner;
        List<Vector3> locations = new List<Vector3>();

        // Start is called before the first frame update
        void Awake()
        {
            spawner = sp.GetComponent<Spawner>();
        }

        void Start()
        {
            slot = PlayerPrefs.GetInt("slot");
            position = new Vector3(0, 0, 0);
            //move camera to initial camera position (slot 0)
            MoveCamera();
            //apply offset
            transform.eulerAngles = transform.eulerAngles - rotOffset;


        }
        // Update is called once per frame
        void LateUpdate()
        {
            //get points of focus
            locations = spawner.GetSpawnLocs();
            focus = SetFocus(slot);
            MoveCamera();
        }

        Vector3 SetFocus(int s)
        {
            Vector3 f = focus;
            f = locations[s] - offset;
            //f = locations[s];
            return f;
        }
        public void CameraMoveRight()
        {
            if (slot < locations.Count - 1)
            {
                slot += 1;
                focus = SetFocus(slot);
            }
            MoveCamera();
        }

        public void CameraMoveLeft()
        {
            if (slot > 0)
            {
                slot -= 1;
                focus = SetFocus(slot);
            }
            MoveCamera();
        }

        void MoveCamera()
        {
            transform.position = Vector3.SmoothDamp(transform.position, focus, ref refPos, smoothSpeed);
            
        }

        void OnDisable()
        {
            PlayerPrefs.SetInt("slot", slot);
        }

        void OnEnable()
        {
            slot = PlayerPrefs.GetInt("slot");
        }
    }
}

