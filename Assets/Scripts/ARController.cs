using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
namespace KeyboardBuilder
{
    public class ARController : MonoBehaviour
    {
        private int slotToLoad;
        public List<Keyboard> inventory = new List<Keyboard>();
        //public GameObject MyObject;
        public ARRaycastManager RaycastManager;

        // Start is called before the first frame update
        void Start()
        {
            slotToLoad = PlayerPrefs.GetInt("slot") - 1;
            foreach (Keyboard k in Resources.LoadAll<Keyboard>(""))
            {
                inventory.Add(k);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                List<ARRaycastHit> touches = new List<ARRaycastHit>();

                RaycastManager.Raycast(Input.GetTouch(0).position, touches, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

                if (touches.Count > 0)
                {
                    GameObject keyboard = Instantiate(Resources.Load("Prefabs/" + inventory[slotToLoad].baseName + "Prefab"), touches[0].pose.position, touches[0].pose.rotation) as GameObject;
                    keyboard.transform.localScale = new Vector3(1, 1, 1);
                }

            }

        }


    }

}
