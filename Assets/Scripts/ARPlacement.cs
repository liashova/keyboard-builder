using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace KeyboardBuilder
{
    public class ARPlacement : MonoBehaviour
    {

        public GameObject arObjectToSpawn;
        public GameObject placementIndicator;
        private GameObject spawnedObject;
        private Pose PlacementPose;
        private ARRaycastManager aRRaycastManager;
        private ARPlaneManager planeManager;
        private bool placementPoseIsValid = false;

        private int slotToLoad;
        public List<Keyboard> inventory = new List<Keyboard>();

        void Start()
        {
            slotToLoad = PlayerPrefs.GetInt("slot") - 1;
            foreach (Keyboard k in Resources.LoadAll<Keyboard>(""))
            {
                inventory.Add(k);
            }
            aRRaycastManager = FindObjectOfType<ARRaycastManager>();

            arObjectToSpawn = Resources.Load("Prefabs/" + inventory[slotToLoad].baseName + "Prefab") as GameObject;
            arObjectToSpawn.transform.localScale  = new Vector3 (1, 1, 1);
        }


        // need to update placement indicator, placement pose and spawn 
        void Update()
        {
            planeManager = GetComponent<ARPlaneManager>();
            if (spawnedObject == null && placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                ARPlaceObject();
            }

            UpdatePlacementPose();
            UpdatePlacementIndicator();


        }
        void UpdatePlacementIndicator()
        {
            if (spawnedObject == null && placementPoseIsValid)
            {
                placementIndicator.SetActive(true);
                placementIndicator.transform.SetPositionAndRotation(PlacementPose.position, PlacementPose.rotation);
            }
            else
            {
                placementIndicator.SetActive(false);
            }
        }

        void UpdatePlacementPose()
        {
            var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
            var hits = new List<ARRaycastHit>();
            aRRaycastManager.Raycast(screenCenter, hits, TrackableType.PlaneWithinBounds);

            placementPoseIsValid = hits.Count > 0;
            if (placementPoseIsValid)
            {
                PlacementPose = hits[0].pose;
            }
        }

        //private void UpdatePlacementPose()
        //{
        //    var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        //    var hits = new List<ARRaycastHit>();
        //    aRRaycastManager.Raycast(screenCenter, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

        //    placementPoseIsValid = hits.Count > 0;
        //    if (placementPoseIsValid)
        //    {
        //        placementPose = hits[0].pose;

        //        var cameraForward = Camera.current.transform.forward;
        //        var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
        //        placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        //    }
        //}

        void ARPlaceObject()
        {
            spawnedObject = Instantiate(arObjectToSpawn, PlacementPose.position, PlacementPose.rotation);
        }


    }
}
