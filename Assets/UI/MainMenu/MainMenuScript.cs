using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{

    private Button learnButton;
    private Button buildButton;
    private Button communityButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    private void OnEnable()
    {
        VisualElement root = GetComponent<UIDocument>().rootVisualElement;


        learnButton = root.Q<Button>("LearnButton");
        buildButton = root.Q<Button>("BuildButton");

        learnButton.clicked += () =>
        {
            Debug.Log("sceneName to load: " + "Scene Name");
            SceneManager.LoadScene("LearnScene");
        };

        buildButton.clicked += () =>
        {
            Debug.Log("sceneName to load: " + "Scene Name");
            SceneManager.LoadScene("BuildScene");
        };


    }

}
