using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class SimulationMenuScript : MonoBehaviour
{

    private Button backButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        VisualElement root = GetComponent<UIDocument>().rootVisualElement;
        backButton = root.Q<Button>("BackButton");

        backButton.clicked += () =>
        {
            Debug.Log("sceneName to load: " + "Scene Name");
            SceneManager.LoadScene("EditorScene");
        };
    }
}
