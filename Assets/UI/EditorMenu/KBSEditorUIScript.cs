using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class KBSEditorUIScript : MonoBehaviour
{

    private DropdownField caseDropdown;
    private DropdownField switchDropdown;
    private Label sceneTitle;
    private Button keycapsButton;
    private Button backButton;
    private Button arViewButton;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        VisualElement root = GetComponent<UIDocument>().rootVisualElement;
        backButton= root.Q<Button>("BackButton");
        caseDropdown = root.Q<DropdownField>("CaseDropdown");
        switchDropdown = root.Q<DropdownField>("SwitchDropdown");
        arViewButton = root.Q<Button>("ARViewButton");


        sceneTitle = root.Q<Label>("SceneTitle");

        backButton.clicked += () =>
        {
            Debug.Log("sceneName to load: " + "Scene Name");
            SceneManager.LoadScene("StartMenuScene");
        };

        arViewButton.clicked += () =>
        {
            Debug.Log("sceneName to load: " + "Scene Name");
            SceneManager.LoadScene("SimulationScene");
        };

        caseDropdown.RegisterValueChangedCallback(ev => setTitle());

    }

    private void setTitle()
    {
        sceneTitle.text = $"{caseDropdown.value}";
    }
}
